"""
Extension classes enhance TouchDesigner components with python. An
extension is accessed via ext.ExtensionClassName from any operator
within the extended component. If the extension is promoted via its
Promote Extension parameter, all its attributes with capitalized names
can be accessed externally, e.g. op('yourComp').PromotedFunction().

Help: search "Extensions" in wiki
"""

from TDStoreTools import StorageManager
TDF = op.TDModules.mod.TDFunctions
import socket
import sys

class SamsungControl:
	"""
	SamsungControl description
	"""
	def __init__(self, ownerComp):
		# The component to which this extension is attached
		self.ownerComp = ownerComp

		# attributes:
		self.a = 0 # attribute
		self.B = 1 # promoted attribute
		self.ContentDB = op.ContentDB
		# properties
		TDF.createProperty(self, 'MyProperty', value=0, dependable=True,
						   readOnly=False)

		# stored items (persistent across saves and re-initialization):
		storedItems = [
			# Only 'name' is required...
			{'name': 'StoredProperty', 'default': None, 'readOnly': False,
			 						'property': True, 'dependable': True},
		]
		# Uncomment the line below to store StoredProperty. To clear stored
		# 	items, use the Storage section of the Component Editor
		
		# self.stored = StorageManager(self, ownerComp, storedItems)

	def myFunction(self, v):
		debug(v)

	def PromotedFunction(self, v):
		debug(v)

	def GetControllerName(self):
		return socket.gethostname()

	def GetIPAddr(self):
		return socket.gethostbyname(self.GetControllerName())


	def SamsungClickEvent(self):
		pickPath = str(op.GUI.op('interact/pickPath')['path', 1])
		mainPage = op.GUI.op('interact').fetch('MAIN_PAGES', [])
		# sceneName = pickPath.split('/')[2]
		screenName = op.GUI.fetch('ACTIVE_SCREEN', None)
		print(pickPath)
		buttonName = pickPath.split('/')[-2]
		settingsScreen = op.GUI.fetch('SettingsScreen', None)

		roomNames = ['chefsCollection', 'dacor', 'familyHub']


		actions = ['pause', 'jump']
		print(buttonName)
		if buttonName in actions:
			if buttonName == 'pause':
				cState = op.GUI.fetch('PAUSE', 0)
				cVal = 1-cState
				op.GUI.store('PAUSE', cVal)
				self.PauseLightingCue(cVal)
				
				op.COMM.op('udpout1').send('{} {} {}'.format(op.GUI.fetch('ACTIVE_SCREEN'), buttonName.capitalize(), str(cVal)))
			elif buttonName == 'jump':
				op.COMM.op('udpout1').send('{} {} {}'.format(op.GUI.fetch('ACTIVE_SCREEN'), buttonName.capitalize(), (-5)))

		if buttonName in roomNames:
			buttonNum = roomNames.index(buttonName)
			op.RoomSelector.clickChild(buttonNum)

		if settingsScreen:
			name = settingsScreen
			op.GUI.store('ACTIVE_SCREEN', name)
			op.GUI.store('SettingsScreen', None)
		elif buttonName in mainPage:
			op.GUI.store('ACTIVE_SCREEN', buttonName)
		# switch(screenName)

		elif buttonName == 'audio':
			ty = op.GUI.op('interact/renderpick1')['ty']
			bar = op(pickPath).parent(2).op('bar')
			
			cVal = ty.eval() + .5
			bar.par.sy = cVal
			self.AudioLevels(cVal)

		elif 'Settings' in buttonName:
			self.OpenSettingsScreen(screen=screenName, button=buttonName)

		elif 'chapter' in buttonName:
			self.SwitchContent(screen=screenName, button=buttonName)


		else:

			print(screenName, buttonName)

	def AudioLevels(self, cVal):
		op.GUI.op('constant1').par.value0 = cVal

	def SwitchContent(self, button=None, screen=None):

		#	print(me, 'ACTIVE_SCREEN', screenName, buttonName)


		familyHubButtons = list(range(0, 14, 2))
		dacorButtons = list(range(6))
		chefsCollection = list(range(0, 9, 2))

		print(screen)
		# if screen == 'familyHu'
		# # print(sys._getframe().f_code.co_name)
		if 'family' in screen:
			activeList = familyHubButtons
		elif 'acor' in screen:
			activeList = dacorButtons

		elif 'ollection' in screen:
			activeList = chefsCollection


		buttonNum = tdu.digits(button)
		clickButton = activeList[buttonNum]
		print(button)
		room = op.GUI.fetch('ACTIVE_SCREEN', None)
		print(room, button)
		# activeDAT = self.ContentDB.op(screen + 'Active')
		#
		# activeDAT.par.dat = screen + 'Chapter' + str(buttonNum)


#		print(clickButton, 'This is the button numb')
#		op.Playlist.SelectRow(clickButton)
		msg = '{} {}'.format(room, button)
		op.COMM.op('lightCueManager').run(button, screen)
		op.COMM.op('udpout1').send(msg)
		return

	def OpenSettingsScreen(self, button=None, screen=None):
		# load settings into settings page
		settingsPage = button.split('Settings')
		op.GUI.op('settings/interactiveMap/setup').run(screen)

		# print(button, screen, settingsDat)
		# self.SwitchContent(button=button, screen=screen)
		op.GUI.store('ACTIVE_SCREEN', 'settings')
		op.GUI.store('SettingsScreen', screen)
		
	def SendLightingCue(self, cueNum=None, button=None, screen=None):
		
		pass
		
	def PauseLightingCue(self, currentVal):
		lightCue = op.GUI.fetch('LightingCues')
		currentRoom = op.GUI.fetch('ACTIVE_SCREEN')
		
		currentCue = lightCue.get(currentRoom)
		if int(currentVal):
			op.COMM.op('oscout1').sendOSC('/hog/playback/halt/', [currentCue])
			print('Current Cue Paused = {}'.format(currentCue))
	
		else:
			op.COMM.op('oscout1').sendOSC('/hog/playback/resume/', [currentCue])
			
		op.GUI.store('LIGHTS_PAUSED', True)
		